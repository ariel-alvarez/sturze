directory.LegislaturaView = Backbone.View.extend({
    render: function () {
        var thisView = this;
        this.$el.html(this.template());
        loadSlider("slider-legislatura","slider-legislatura");

        jQuery("#navigation-images").find("a").show();
        jQuery("#legislatura-navigation-box").hide();

        var seccion = findUrlParameter("seccion");
        if(!seccion){
            seccion = Categorias.LEGISLATURA;
        }
        this.renderList(seccion);

        $("#legislatura-navigator").find("> li").click(function(){
            $("#legislatura-navigator").find("> li").removeClass("seleccionada");
            var self = $(this);
            self.addClass("seleccionada");
            if(self.data("seccion")){
                thisView.renderList(self.data("seccion"));
            }else{
                thisView.renderList(Categorias.LEGISLATURA);
            }
        });

        HideTwitterFeedAndShowNavigation();
        return this;
    },
    renderList : function(category){
        var listId = "legislatura-post-list";
        $("#"+listId).html("");
        PostsHome.findByCategory(category, function(col){
            if(col.isEmpty()){
                $("#"+listId).append($("<div class='blog-title col-md-12 text-center'>NO SE HAN ENCONTRADO ENTRADAS PARA ESTA CATEGORÍA</div>"));
            }else{
                Paginador.paginar(col, "#legislatura?seccion="+category+"&pagina=", "legislatura-post-list", "legislatura-paginator");
            }

        });
    }
});
