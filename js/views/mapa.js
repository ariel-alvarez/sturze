var Provincia = Backbone.Model.extend({
    defaults: {
        nombre: 'Buenos Aires',
        codigo: 0,
        trabajos: []
    }
});

directory.MapaView = Backbone.View.extend({

    render: function () {
        var self = this;
        this.$el.html(this.template());

        var map = new jvm.WorldMap({
            map: 'ar_mill_en',
            container: $('#mapa-argentina'),
            color:"#FDE880",
            hoverColor:"#FFC61E",
            backgroundColor:"#FFFFFF",
            series: {
                regions: [{
                    attribute: 'fill'
                }]
            },
            onRegionClick:function(event,cod){
                if(self.eventosMap[cod]){
                    $("#provincia_name").text(self.eventosMap[cod].nombre);
                    $("#events_number").text(self.eventosMap[cod].trabajos.length);
                    var relacionado = $("#eventos_container").html('');
                    _.each(self.eventosMap[cod].trabajos, function(post){
                        var lateralExcerpt = $('<div class="col-md-12 lateral-excerpt"></div>');
                        if(post.getImageUrl()) {
                            lateralExcerpt.append($("<div class='col-md-3 excerpt-image-small'></div>")
                                .css("background","url('" + post.getImageUrl() +"') 50% 50% / cover no-repeat")
                                .css('margin-right','10px'));
                        }
                        lateralExcerpt.append($(post.getTitleAsAnchor()));
                        relacionado.append(lateralExcerpt);
                    });
                }
            }
        });
        var generateColors = function(){
            var colors = {};
            for (var key in map.regions) {
                colors[key] = '#e0e0e0';
            }
            return colors;
        };
        map.series.regions[0].setValues(generateColors());

        new directory.PostsMapa().fetch({success:function(col){
            console.log(col);
            self.renderMap(map, col.models);
        }});
        return this;
    },
    eventosMap: {},
    renderMap: function(map, col){
        var self=this;

        _.each(MAPA_ARGENTINA.paths, function(provincia,cod){
            self.eventosMap[cod] = new Provincia({nombre: provincia.name, codigo: cod, trabajos:[]}).toJSON()
        });

        _.each(col, function(post){
            _.each(post.getProvincias(), function(cod){
                if(!self.eventosMap[cod])return;
                self.eventosMap[cod].trabajos.push(post);
            });
        });

        var provinciasConEventos = {};
        _.each(self.eventosMap,function(provincia, cod){
            if(provincia.trabajos.length >0){
                provinciasConEventos[cod] = '#FFD302';
            }else{
                provinciasConEventos[cod] = "#FDE880";
            }
        });
        map.series.regions[0].setValues(provinciasConEventos);
        console.log(self.eventosMap);
//          accordion.append(PROVINCIA_TPL(p))


    }
});
