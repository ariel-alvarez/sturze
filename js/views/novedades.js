directory.NovedadesView = Backbone.View.extend({

    events:{
        "click #novedades-search-button": "onSearchClick",
        "keyup #novedades-search-button": "onSearchClick"
    },
    initialize: function(){
        this.filtered = [];
        return this;
    },
    hasFiltered:function(){ return this.filtered.length != 0},
    renderList : function(col, year){
        this.$el.html(this.template());
        this.renderPaginatedList(col, year);
        return this;
    },
    renderSearchList: function(query){
        var self = this;
        var col = new (directory.SearchPostCollection(query))();
        col.fetch({success:function(result){
            self.renderList(result);
        }});
    },
    render: function () {
        var self = this;
        this.$el.html(this.template());
        var year = Number(findUrlParameter("year"));
        if(year){
            this.search(year);
        }else{
            PostsHome.findByCategories([Categorias.BLOG, Categorias.PRENSA, Categorias.LEGISLATURA], function(col){
                self.renderPaginatedList(col);
                jQuery("#novedades-post-list").append(TWITTER_SHARE_BUTTON);
            });
        }

        HideTwitterFeedAndShowNavigation();
        return this;
    },
    search: function(year){
        var self = this;
        PostsHome.all(function(col){
            self.renderList(col.yearFilter(year), year);
        });
    },
    onSearchClick: function(){
        var year = Number(jQuery("#novedades-search-input").val());
        if(year){
            this.search(year);
        }
    },
    renderPaginatedList:function(col,searchYear){
        Paginador.paginar(col, "#novedades?" + (searchYear?"year=" +searchYear+"&" : "") + "pagina=", "novedades-post-list", "novedades-paginator");
    }
});

var TWITTER_SHARE_BUTTON = "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
