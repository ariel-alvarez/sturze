directory.HomeView = Backbone.View.extend({

    events:{
        "click #showMeBtn":"showMeBtnClick"
    },

    initialize:function(){
        PostsHome.all(); //initialize PostsHome
    },

    renderDestacados: function(col){
        var large = col.take(7);
        buildPostBox("#destacado-1", large[0], COLORES.BLANCO);
        buildPostBox("#destacado-2", large[1], COLORES.GRIS_CLARO);
        buildPostBox("#destacado-3", large[2], COLORES.GRIS_OSCURO);
        buildPostBox("#destacado-4", large[3], COLORES.AMARILLO);
        buildPostBox("#destacado-5", large[4], COLORES.GRIS_CLARO);
        buildPostBox("#destacado-6", large[5], COLORES.AMARILLO);
        buildPostBox("#destacado-7", large[6], COLORES.BLANCO);
    },
    renderLarge: function(col){
        var large = col.first();
        var selector = "#destacado-large-1";

        buildPostBox(selector, large, '#FFCC00', true);

    },
    render:function () {
        this.$el.html(this.template());
        $("#banner-mis-publicaciones").show();
        loadSlider("slider-home", "slider-home");
        var self = this;
        if(!PostsHome.store.cargada){
            new directory.PostsDestacados().fetch({success:function(col){
                self.renderDestacados(col);
            }});
        }else{
            PostsHome.findByCategory(Categorias.DESTACADO, function(col){
                self.renderDestacados(col);
            });
        }

        if(!PostsHome.store.cargada){
            new (directory.PostsCollection(Categorias.DESTACADO_LARGE, 1))().fetch({success:function(col){
                self.renderLarge(col);
            }});
        }else{
            PostsHome.findByCategory(Categorias.DESTACADO_LARGE, function(col){
                self.renderLarge(col);
            });
        }

        $('#propuesta-link').click(function(){
            $('#propuesta-form-container').toggle(600);
        });

        $('#resumen-bio-link').click(function(){
            $('#resumen-biografico').toggle(400);
        });

        $.validate({
            modules : 'date',
            form : '#propuesta-form'
        });

        $("#propuesta-form").submit(function(e)
        {

            e.preventDefault(); //STOP default action

            var url = $(this).attr("action");
            var data = $(this).serializeArray();
            $.validate({
                modules : 'date',
                form : '#propuesta-form',
                onSuccess : function() {
                    $.ajax(
                        {
                            url : url,
                            type: "POST",
                            data : data,
                            success:function(data, textStatus, jqXHR)
                            {
                                $("#propuesta-form-container").html("").append("<div class='text-center' style='font-size: 24px;'>Hemos recibido tu propuesta. Gracias por formar parte!</div>");

                            },
                            error: function(jqXHR, textStatus, errorThrown)
                            {
                                $("#propuesta-form-container").html("").append("<div class='text-center' style='font-size: 24px;'>Hemos recibido tu propuesta. Gracias por formar parte!</div>");
                            }
                        });
                    return false;
                }
            });
        });

        RenderTwitter();
        return this;
    },

    showMeBtnClick:function () {
        directory.shellView.search();
    }
});

function buildPostBox(selector, post, fallbackColor, noLengthLimitation){
    if(setBackgroundImage(selector, post, fallbackColor)){
        $(selector)
            .addClass("box-post-imagen post-novedad-box")
            .append($("<div class='box-noticia-date-category row '></div>")
                .append($("<div class='box-noticia-category invert' style='text-align: left; float:left'></div>").text(post.getFechaPublicacion()))
                .append($("<div class='box-noticia-category invert' style='float:right'></div>").text(post.getMainCategory().toUpperCase()))
            )
            .append($('<div class="post-novedades-title text-center"></div>')
                .append(post.getTitleAsAnchor())
            );
        return true;
    }else{
        jQuery(selector).addClass("box-post-texto");
        renderNoticiaBox(selector, post, noLengthLimitation);
        return false;
    }
}

function setBackgroundImage(selector, imageOption, fallbackColor){
    var box = jQuery(selector);
    if(imageOption && imageOption.get("thumbnail_images")){
        var images = imageOption.get("thumbnail_images");
        var image = images.large? images.large : images.full;
        box.css("background", 'url("'+image.url+'") no-repeat 50% 50%').css("background-size", 'cover');
        return true;
    } else {
        box.css("background-color", fallbackColor? fallbackColor : '#E5E5E5');
        return false;
    }
}

function generateTextContainer(textoLargo, url, noLengthLimitation){
    var textoContainer = $("<div class='box-noticia-texto'></div>").append(noLengthLimitation?textoLargo:limitText(textoLargo, 300));
    textoContainer.find('p').last().append($("<a href='#"+url+"'>[...]</a>"));
    return textoContainer;
}
var renderNoticiaBox = function(selector, post, noLengthLimitation){

    $(selector)
        .append($("<div class='box-noticia-date-category row'></div>")
            .append($("<div class='box-noticia-category' style='text-align: left; float:left'></div>").text(post.getFechaPublicacion()))
            .append($("<div class='box-noticia-category' style='float:right'></div>").text(post.getMainCategory().toUpperCase()))
        )
        .append($("<div class='box-noticia-titulo'></div>").append(post.getTitleAsAnchor()))
        .append($("<div class='box-noticia-bajada'></div>").text(""))
        .append(generateTextContainer( post.get("excerpt"), post.getPostUrl(), noLengthLimitation));
};

function limitText(text, characters){
    var max = characters? characters : 180;
    if (text.length < max) return text;
    var tail = text.slice(max).indexOf(" ");
    if(tail > 0) return text.slice(0,180+tail);
    console.log(text);
    return text;
}
